from typing import Callable, Iterator


def is_close(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


# it seems that values in the table with examples rounded between iteration's steps
def Euler(x_0: float, y_0: float, b: float, h: float,
          derivative: Callable, exact: Callable) -> Iterator:
    last_y = y_0
    last_x = x_0
    x_i = last_x + h
    yield x_0, y_0, y_0, 0, 0
    while x_i < b:
        x_i = last_x + h
        y_i = round(last_y + h * derivative(last_x, last_y), 3)
        y_exact = round(exact(x_i), 3)
        exact_last_x = round(exact(last_x), 3)
        LTE = round(y_exact - exact_last_x - h * derivative(last_x, exact_last_x), 4)
        GTE = round(y_exact - y_i, 3)
        last_y = y_i
        last_x = x_i
        yield x_i, y_exact, y_i, LTE, GTE


def Improved_Euler(x_0: float, y_0: float, b: float, h: float,
                   derivative: Callable, exact: Callable) -> Iterator:
    last_y = y_0
    last_x = x_0
    x_i = last_x + h
    half_h = h / 2
    yield x_0, y_0, y_0, 0, 0
    while x_i < b:
        x_i = last_x + h
        K_1i = derivative(last_x, last_y)
        K_2i = derivative(x_i, last_y + h * K_1i)
        y_i = last_y + half_h * (K_1i + K_2i)
        y_exact = exact(x_i)
        exact_last_x = exact(last_x)
        derivative_last_x_exact_last_x = derivative(last_x, exact_last_x)
        LTE = y_exact - exact_last_x - half_h * (derivative_last_x_exact_last_x +
                                                 derivative(x_i, exact_last_x + h * derivative_last_x_exact_last_x))
        GTE = y_exact - y_i
        last_y = y_i
        last_x = x_i
        yield x_i, y_exact, y_i, LTE, GTE


def Runge_Kutta(x_0: float, y_0: float, b: float, h: float,
                derivative: Callable, exact: Callable) -> Iterator:
    last_y = y_0
    last_x = x_0
    x_i = last_x + h
    half_h = h / 2
    h_over_6 = h / 6
    yield x_0, y_0, y_0, 0, 0
    while x_i < b:
        x_i = last_x + h
        K_1i = derivative(last_x, last_y)
        K_2i = derivative(last_x + half_h, last_y + half_h * K_1i)
        K_3i = derivative(last_x + half_h, last_y + half_h * K_2i)
        K_4i = derivative(last_x + h, last_y + h * K_3i)
        y_i = last_y + h_over_6 * (K_1i + 2 * K_2i + 2 * K_3i + K_4i)
        y_exact = exact(x_i)
        exact_last_x = exact(last_x)
        # computing errors
        k1 = derivative(last_x, exact_last_x)
        k2 = derivative(last_x + h / 2, exact_last_x + h * k1 / 2)
        k3 = derivative(last_x + h / 2, exact_last_x + h * k2 / 2)
        k4 = derivative(last_x + h, exact_last_x + h * k3)
        LTE = y_exact - exact_last_x - h * (k1 + 2 * k2 + 2 * k3 + k4) / 6
        GTE = y_exact - y_i
        last_y = y_i
        last_x = x_i
        yield x_i, y_exact, y_i, LTE, GTE


def print_table(x_0: float, y_0: float, b: float, h: float,
                func: Callable, derivative: Callable, exact: Callable) -> None:
    print(f"h = {round(h, 2)}")
    func_len = len(func.__name__) + 6
    print(f"x      y(exact)   y({func.__name__})   LTE      GTE")
    for x_i, y_exact, y_i, LTE, GTE in func(x_0, y_0, b, h, derivative, exact):
        x_i = str(round(x_i, 2))
        y_exact = str(round(y_exact, 3))
        y_i = str(round(y_i, 3))
        LTE = str(round(LTE, 4))
        GTE = str(round(GTE, 3))
        print(f"{x_i}" + " " * (7 - len(x_i)) +
              f"{y_exact}" + " " * (11 - len(y_exact)) +
              f"{y_i}" + " " * (func_len - len(y_i)) +
              f"{LTE}" + " " * (9 - len(LTE)) +
              f"{GTE}")
    print()


def print_h_comparison_results(x_0: float, y_0: float, b: float, h1: float, h2: float,
                               h1_index: int, h2_index: int, pow1: int, pow2: int,
                               func: Callable, derivative: Callable, exact: Callable) -> None:
    print(f"{func.__name__}; h{h1_index} = {h1}; h{h2_index} = {h2}")
    h = round(h1 / h2)
    print(f"Changing step size h in h{h1_index} / h{h2_index} = {h1} / {h2} = {h} times")

    generator_for_h1 = func(x_0, y_0, b, h1, derivative, exact)
    generator_for_h2 = func(x_0, y_0, b, h2, derivative, exact)

    next(generator_for_h1)
    next(generator_for_h2)

    print(f"x      LTE{h1_index} / LTE{h2_index}   GTE{h1_index} / GTE{h2_index}")
    x_0 = str(round(x_0, 2))
    print(f"{x_0}" + " " * (7 - len(x_0)) +
          f"0" + " " * 13 +
          f"0")

    while True:
        try:
            x_i_h1, _, _, LTE_h1, GTE_h1 = next(generator_for_h1)
        except StopIteration:
            break
        x_i_h2, _, _, LTE_h2, GTE_h2 = next(generator_for_h2)
        while not is_close(x_i_h1, x_i_h2):
            x_i_h2, _, _, LTE_h2, GTE_h2 = next(generator_for_h2)

        x_i_h1 = str(round(x_i_h1, 2))
        LTE = str(round(LTE_h1 / LTE_h2, 3))
        GTE = str(round(GTE_h1 / GTE_h2, 3))
        print(f"{x_i_h1}" + " " * (7 - len(x_i_h1)) +
              f"{LTE}" + " " * (14 - len(LTE)) +
              f"{GTE}")
    s = f"^{pow2}" if pow2 != 1 else ""
    print(f"O(h^{pow1}) ~ {round(h ** pow1)}; O(h" + s + f") ~ {round(h ** pow2)}")
    print()


def exact(x: float) -> float:
    return x * (1 + x ** 2 / 3) / (1 - x ** 2 / 3)


def derivative(x: float, y: float) -> float:
    return (y ** 2 + x * y - x ** 2) / (x ** 2)


def main():
    h1 = 0.1
    h2 = 0.05
    h3 = 0.01
    x_0 = 1
    y_0 = 2
    b = 1.5

    print("Abuzyar Tazetdinov BS19-04\n")

    print_table(x_0, y_0, b, h1, Euler, derivative, exact)
    print_table(x_0, y_0, b, h2, Euler, derivative, exact)
    print_table(x_0, y_0, b, h3, Euler, derivative, exact)
    print_h_comparison_results(x_0, y_0, b, h1, h2, 1, 2, 2, 1, Euler, derivative, exact)
    print_h_comparison_results(x_0, y_0, b, h1, h3, 1, 3, 2, 1, Euler, derivative, exact)

    print_table(x_0, y_0, b, h1, Improved_Euler, derivative, exact)
    print_table(x_0, y_0, b, h2, Improved_Euler, derivative, exact)
    print_table(x_0, y_0, b, h3, Improved_Euler, derivative, exact)
    print_h_comparison_results(x_0, y_0, b, h1, h2, 1, 2, 3, 2, Improved_Euler, derivative, exact)
    print_h_comparison_results(x_0, y_0, b, h1, h3, 1, 3, 3, 2, Improved_Euler, derivative, exact)

    print_table(x_0, y_0, b, h1, Runge_Kutta, derivative, exact)
    print_table(x_0, y_0, b, h2, Runge_Kutta, derivative, exact)
    print_table(x_0, y_0, b, h3, Runge_Kutta, derivative, exact)
    print_h_comparison_results(x_0, y_0, b, h1, h2, 1, 2, 5, 4, Runge_Kutta, derivative, exact)
    print_h_comparison_results(x_0, y_0, b, h1, h3, 1, 3, 5, 4, Runge_Kutta, derivative, exact)


if __name__ == '__main__':
    main()
