def exact(x):
    return x * (1 + x ** 2 / 3) / (1 - x ** 2 / 3)


def derivative(x, y):
    return (y ** 2 + x * y - x ** 2) / (x ** 2)


h = 0.1
x_0 = 1
b = 1.5
y_0 = 2

last_y = y_0
last_x = x_0
x_i = last_x + h
print(f"Euler: y({x_0}) = {y_0:.2f}; "
      f"Exact: y({x_0}) = {y_0:.2f}; "
      f"TE = 0")
while x_i < b:
    x_i = last_x + h
    y_i = last_y + h * derivative(last_x, last_y)
    y_exact = exact(x_i)
    print(f"Euler: y({x_i:.1f}) = {y_i:.2f}; "
          f"Exact: y({x_i:.1f}) = {y_exact:.2f}; "
          f"TE = {y_exact - y_i:.2f}")
    last_y = y_i
    last_x = x_i
